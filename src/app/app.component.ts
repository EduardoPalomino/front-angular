import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  courseName: string = '';
  days: any = {
    lunes: false,
    martes: false,
    miercoles: false,
    jueves: false,
    viernes: false,
    sabado: false,
    domingo: false
  };
  startTime: number = 8;
  endTime: number = 9;
  hours: number[] = [8, 9, 10, 11, 12, 13, 14, 15, 16];
  week: string[] = ['lunes', 'martes', 'miercoles', 'jueves', 'viernes', 'sabado', 'domingo'];
  schedule: any = {};
  courses: string[] = [
    'Hab.Lógico-Matemático', 
    'Hab.Verbal', 
    'Raz.Matemático', 
    'Raz.Verbal', 
    'Anatomía', 
    'Biología', 
    'Ciencias', 
    'Física', 
    'Química', 
    'Cívica', 
    'Economía', 
    'Filosofía', 
    'Geografía', 
    'Historia', 
    'Historia-del-Perú', 
    'Historia-Universal', 
    'Humanidades', 
    'Lectura-Comprensiva-e-Interpretativa', 
    'Lectura-Critica', 
    'Lenguaje', 
    'Literatura', 
    'Ortografía-y-Puntuación', 
    'Psicología', 
    'Vocabulario', 
    'Alemán', 
    'Frances', 
    'Ingles', 
    'Álgebra', 
    'Aritmética', 
    'Estadística', 
    'Geometría', 
    'Matemáticas', 
    'Números-y-Operaciones', 
    'Trigonometría', 
    'Examen', 
    'Simulacro'
  ];
  
// **Mapa de colores para los cursos**
courseColors: any =  {
  'Hab.Lógico-Matemático': 'color-hab-logico-matematico',
  'Hab.Verbal': 'color-hab-verbal',
  'Raz.Matemático': 'color-raz-matematico',
  'Raz.Verbal': 'color-raz-verbal',
  'Anatomía': 'color-anatomia',
  'Biología': 'color-biologia',
  'Ciencias': 'color-ciencias',
  'Física': 'color-fisica',
  'Química': 'color-quimica',
  'Cívica': 'color-civica',
  'Economía': 'color-economia',
  'Filosofía': 'color-filosofia',
  'Geografía': 'color-geografia',
  'Historia': 'color-historia',
  'Historia-del-Perú': 'color-historia-del-peru',
  'Historia-Universal': 'color-historia-universal',
  'Humanidades': 'color-humanidades',
  'Lectura-Comprensiva-e-Interpretativa': 'color-lectura-comprensiva-e-interpretativa',
  'Lectura-Critica': 'color-lectura-critica',
  'Lenguaje': 'color-lenguaje',
  'Literatura': 'color-literatura',
  'Ortografía-y-Puntuación': 'color-ortografia-y-puntuacion',
  'Psicología': 'color-psicologia',
  'Vocabulario': 'color-vocabulario',
  'Alemán': 'color-aleman',
  'Frances': 'color-frances',
  'Ingles': 'color-ingles',
  'Álgebra': 'color-algebra',
  'Aritmética': 'color-aritmetica',
  'Estadística': 'color-estadistica',
  'Geometría': 'color-geometria',
  'Matemáticas': 'color-matematicas',
  'Números-y-Operaciones': 'color-numeros-y-operaciones',
  'Trigonometría': 'color-trigonometria',
  'Examen': 'color-examen',
  'Simulacro': 'color-simulacro'
};

  addCourse() {
    const selectedDays = Object.keys(this.days).filter(day => this.days[day]);

    if (!this.courseName || selectedDays.length === 0 || this.startTime >= this.endTime) {
      alert('Por favor, completa todos los campos correctamente.');
      return;
    }

    selectedDays.forEach(day => {
      for (let hour = this.startTime; hour < this.endTime; hour++) {
        if (!this.schedule[hour]) {
          this.schedule[hour] = {};
        }
        this.schedule[hour][day] = this.courseName;
      }
    });
  }

  getClass(hour: number, day: string): string {
    // **Obtiene el nombre del curso**
    const course = this.schedule[hour] && this.schedule[hour][day] ? this.schedule[hour][day] : '';
    // **Devuelve la clase correspondiente al curso**
    return course ? this.courseColors[course] : '';
  }

  getCourse(hour: number, day: string): string {
    return this.schedule[hour] && this.schedule[hour][day] ? this.schedule[hour][day] : '';
  }
}
